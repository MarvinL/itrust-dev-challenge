# ITrust Dev Challenges

## Temps passé sur le test

Temps de R&D: 2 j.h

Temps de réalisation du POC: 1/2 j.h

Temps de rédaction de la documentation: < 1/2 j.h

## Objectif du test

L'objectif principal du test est principalement de faire des choix techniques et de les justifier. En second temps, la réalisation d'un POC vient compléter les choix effectués. 

## Choix des technologies

Comme le stipule l'énoncé du test, la technologie imposée est React.

Concernant les autres choix techniques, voici le détail des choix que j'ai pu prendre.

### Librairie de manipulation des horodatages

Puisque le coeur de l'application est de manipuler des dates et des temps, le choix d'une librairie de manipulation d'horodatages est indispensable. 

Pour que cela puisse être lié à React, le premier critère est la compatibilité avec ce framework. Les autres critères sont les suivants : 
 - Maintenance de la librairie
 - Recommandation par la communauté React
 - Communauté utilisant la librairie
 - Documentation
 - Internationalisation

Avec ces critères, la librairie qui est ressortie immédiatement est [Moment.js](https://momentjs.com/). Cette dernière permet la manipulation de date sous différents formats et permettant des comparaisons de date assez simplifiée.

### Librairie de composant graphique

En complément de la librairie d'horodatage, une librairie de composant graphique est indispensable pour pouvoir créer une interface orientée utilisateur. 

Les critères de selections sont les suivants:
 - Composants disponibles
 - Documentation claire et avec exemple d'utilisation
 - Communauté développeur
 - Maintenance et évolution
 - Possibilité de créer des templates et des styles personnalisés

Les différentes librairies de composant graphique les plus utilisés sont:
 - [Material-UI](https://material-ui.com/)
 - [React Bootstrap](https://react-bootstrap.github.io/)
 - [Bulma](https://bulma.io/)
 - [Ant Design](https://ant.design/)

Au vu des différents critères, mon choix s'est porté sur Ant Design pour ses exemples et la facilité d'implémenter une interface s'adaptant à la taille de l'écran. Les différents flux de données peuvent être intégrés aux composants déjà existant.

> Attention, sur ce POC, l'interface n'est pas totalement reactive et adaptative sur tous les écrans (par exemple mobile).

L'interface livrée avec ce test contient seulement une visualisation du tableau d'évènements horodatés et la modale permettant de créer un évènement horodaté. Les bouton 'Modifier' et 'Supprimer' ne sont pas fonctionnels.

### Pour une réalisation complète de cette application

Viennent s'ajouter à tous ces choix différents outils indispensables à la bonne réalisation du développement de l'application comme:
 - [Redux](https://redux.js.org/) & [React-router](https://reacttraining.com/react-router/web/guides/quick-start)
 - [React Hook Form](https://react-hook-form.com/) & [React Hook Form Input](https://github.com/react-hook-form/react-hook-form-input) (permet d'avoir un système de formulaire très rapide et utilisable avec les Hooks)
 - Une API permettant de restituer tous les évènements (ici les évènements sont entrés en dur dans l'application)
 - Et bien d'autres pour venir créer une application complète

## Organisation dans le travail

### Conception du format de données

Pour avoir une cohérence Back/Front, le formatage des données est très important. Cette étape est essentielle pour qu'aucune erreur de format vient perturbée le développement de l'application. 

Cette conception nécessite un lien avec des personnes travaillant sur les applications existantes pour avoir une cohérence complète des données et pour pouvoir fixer ce format dans les bases de données et dans les API.

### Planning de recherches & développement

Pour partir sur une base saine de développement, les technologies choisies doivent tout d'abord être testé et confirmé par toutes l'équipe de développement.

Dès que ces technologies sont confirmés et après définition du cahier des charges, les itérations peuvent commencé. Voici les différentes étapes:
 - Conception de User Stories & conception de l'interface (Sprint 1)
 - Itération sur l'interface (si nécessaire) & début du développement (Sprint 2)
 - Développement de l'interface avec bouchon (si l'API n'existe pas ou est en cours de développement) (Sprint 3-4)
 - Liaison avec le Back-end et livraison de l'application (Sprint 5+)

## Lancement du POC

Pour pouvoir lancer l'application, il vous faudra au préalable cloner le repository GitLab du projet (avec l'outil de votre choix).

Ensuite, veillez à installer les dépendances manquantes avec ```npm install``` en vous plaçant dans le dossier itrust contenant le `package.json`

Dès que ces dépendances sont installées, lancez la commande ```npm start``` et rendez vous dans votre navigateur pour essayer le POC.

> Note : Ce POC contient quelques petits bugs pouvant être résolu facilement mais au vu de la date de rendu demandé, ces  bugs ne sont pas fixé dans la version actuelle du POC.