import React, { useState } from 'react';
import { Table, Button, Tooltip, Tag, Row, Col, DatePicker, TimePicker, Select } from 'antd';
import { EditOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Modal, Input, Form } from 'antd';

const tagsData = ['Event', 'Date', 'Time'];

export default function TableEvents() {
    const [modal, setModal] = useState(false);
    const [tags, setTags] = useState([]);
    const [data, setData] = useState([
        {
            key: '0',
            event: 'Event 1',
            date: '2020-03-11',
            time: '11:27:53',
            tags: []
        },
        {
            key: '1',
            event: 'Event 2',
            date: '2020-03-11',
            time: '11:27:53',
            tags: []
        },
        {
            key: '2',
            event: 'Event 3',
            date: '2020-03-11',
            time: '11:27:53',
            tags: ['Date', 'Time', 'Event']
        },
        {
            key: '3',
            event: 'Event 4',
            date: '2020-03-11',
            time: '11:27:53',
            tags: []
        },
    ]);

    const columns = [
        {
            title: 'Event name',
            dataIndex: 'event',
            key: 'event',
        },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        },
        {
            title: 'Time',
            dataIndex: 'time',
            key: 'time',
        },
        {
            title: 'Tags',
            dataIndex: 'tags',
            key: 'tags',
            render: tags => (
                tags && <span>
                    {
                        tags.map(tag => 
                            <Tag key={tag}>
                                {tag.toUpperCase()}
                            </Tag>
                        )
                    }
                </span>
            )
        },
        {
            title: 'Actions',
            key: 'action',
            render: () => (
                <span>
                    <Tooltip title="modify">
                        <Button type="primary" shape="circle" icon={<EditOutlined/>} />
                    </Tooltip>
                    <Tooltip>
                        <Button type="primary" danger shape="circle" icon={<DeleteOutlined/>} />
                    </Tooltip>
                </span>
            )
        }
    ];
    
    return (
        <div>
            <Row key={1} gutter={[8, 16]}>
                <Col span={19} />
                <Col span={1} >
                    <Button shape="circle" icon={<PlusOutlined/>} onClick={() => setModal(true)}/>
                </Col>
                <Col span={4} />
            </Row>
            <Row key={2} gutter={[8, 16]}>
                <Col span={4} />
                <Col span={16} >
                    <Table columns={columns} dataSource={data}/>
                </Col>
                <Col span={4} />
            </Row>

            <Modal visible={modal} footer={null}
            onCancel={() => {
                setTags([]);
                setModal(false);
            }}>
                <Form onFinish={fieldsValue => {
                        setData(prevArray => [...prevArray, {...fieldsValue, date: fieldsValue.date._d.toString().substring(4, 15), time: fieldsValue.time._d.toString().substring(16, 24), key:data.length + 1}]);
                        setModal(false);
                    }
                }
                >
                    <Form.Item
                        label="Event name"
                        name="event"
                        rules={[{ required: true, message: 'Please input an event name'}]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Date"
                        name="date"
                    >
                        <DatePicker />
                    </Form.Item>
                    <Form.Item
                        label="Time"
                        name="time"
                    >
                        <TimePicker/>
                    </Form.Item>
                    <Form.Item
                        label="Tags"
                        name="tags"
                    >
                        <Select
                            mode="multiple"
                            placeholder="Choose some tags"
                            value={tags}
                            onChange={selectedItems => setTags(selectedItems)}
                            style={{ width: '200px' }}
                        >
                            {
                                tagsData.filter(o => !tags.includes(o)).map(item => (
                                    <Select.Option key={item} value={item}>
                                        {item}
                                    </Select.Option>
                                ))
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Create
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}
