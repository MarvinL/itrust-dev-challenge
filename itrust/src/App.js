import React from 'react';
import TableEvents from './components/contentComponents/TableEvents';

import 'antd/dist/antd.css';
import './App.css';

import { Layout } from 'antd';
const { Header, Footer, Content } = Layout;

function App() {
  return (
    <div className="App">
      <Layout>
        <Header>
          
        </Header>
        <Content>
          <TableEvents/>
        </Content>
        <Footer>

        </Footer>
      </Layout>
    </div>
  );
}

export default App;
